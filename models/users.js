var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    email: { type: String, required: [true, "can't be blank"] },
    firstname: { type: String, required: [true, "can't be blank"] },
    lastname: { type: String, required: [true, "can't be blank"] },
    age: {type: Number}
}, { timestamps: true });

module.exports = { users: mongoose.model('users', UserSchema) };
