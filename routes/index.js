const express = require('express'),
      router = express.Router(),
      mUser = require('../models/users').users,
      XLSX = require('xlsx'),
      _ = require('underscore'),
      nodemailer = require('nodemailer');

var transport = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "34e9763e3@cd7e",
    pass: "f180e352d5730f4"
  }
});

const multer = require('multer')
// Image Upload
const imageStorage = multer.diskStorage({
  destination: 'files',
  filename: (req, file, cb) => {
    cb(null, file.originalname)
  }
});

const imageUpload = multer({
  storage: imageStorage,
  limits: {
    fileSize: 1000000  
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(csv)$/)) {
      return cb(new Error('Please upload csv file'))
    }
    cb(undefined, true)
  }
})

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

//create users
router.post('/create', function (req, res, next) {
  let { firstname, lastname, email, age } = req.body;
  let userData = {
    firstname: firstname,
    lastname: lastname,
    email: email,
    age: age
  }
  mUser.create(userData, (uErr, uData) => {
    if (uErr) {
      res.status(500).send({ err: null, data: uErr });
    } else {
      res.setHeader("Content-Type", "application/json");
      res.status(200).send({ err: null, data: uData });
    }
  });
});

//Send email
router.post('/email', imageUpload.single('csv_file'), (req, res) => {
  let resData = [], Error = [];
  let workbook = XLSX.readFile(req.file.path);
  let workSheetNames = workbook.SheetNames;
  if (workSheetNames && workSheetNames.length > 0) {
    for (let i = 0; i < workSheetNames.length; i++) {
      let sheetData = {
        sheetName: workSheetNames[i],
        sheetId: [i],
        headerData: []
      };
      let emailCnt = XLSX.utils.sheet_to_json(workbook.Sheets[workSheetNames[i]], { header: 0, defval: null, blankrows: false })
      _.each(emailCnt, (val, key) => {
        let message = {
          from: "leeluma13ramar@gmail.com",
          to: val.email,
          subject: "Newsletter Email Test",
          text: `Hi ${val.name} \n ${val.content}`
        }
        transport.sendMail(message, function (err) {
          if (err) {
            Error.push(val.email);
            if (emailCnt.length === (key + 1)) {
              res.status(500).send({ err: `Can't to sent email,Please check all the content of ${(Error.join(','))}`, data: null });
            }
          } else {
            if (emailCnt.length === (key + 1)) {
              res.status(200).send({ err: null, data: 'success' });
            }
          }
        });
      });
      resData.push(sheetData);
    }
  } else {
    res.status(200).send({ err: null, data: null });
  }
}, (error, req, res, next) => {
  res.status(400).send({ error: error.message })
})
module.exports = router;
